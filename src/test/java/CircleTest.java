import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CircleTest {
    Point center = new Point(0, 0);
    Circle circle = new Circle(center, 10);

    @Test
    void circleIncludesCenterPoint() {
        Assertions.assertTrue(circle.includes(center), "Center should be always in the circle");
    }

    @Test
    void circleIncludesPoint() {
        Point point = new Point(7, 5);
        Assertions.assertTrue(circle.includes(point));
    }

    @Test
    void circleNotIncluedPoint() {
        Point point2 = new Point(2, 14);
        Assertions.assertTrue(!circle.includes(point2));
    }

    @Test
    void circleShouldIncludeBorderPoints() {
        Point north = new Point(0, 10);
        Assertions.assertTrue(circle.includes(north));

        Point south = new Point(0, -10);
        Assertions.assertTrue(circle.includes(south));

        Point east = new Point(-10, 0);
        Assertions.assertTrue(circle.includes(east));

        Point west = new Point(10, 0);
        Assertions.assertTrue(circle.includes(west));

    }

}