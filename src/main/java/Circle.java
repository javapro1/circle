import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Circle {

    @Override
    public String toString() {
        return "Circle{" +
                "center=" + center +
                ", radius=" + radius +
                '}';
    }

    Point center;
    private double radius;

    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    public boolean includes(Point point) {
        return Math.sqrt((Math.pow((point.getX() - center.getX()), 2)
                + Math.pow((point.getY() - center.getY()), 2))) <= radius;
    }
}
