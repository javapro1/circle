import lombok.Setter;
import lombok.Getter;

import java.util.Scanner;

@Setter
@Getter

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Point[] points = setPoints(input);

        System.out.println("Введите параметры окружности: ");
        System.out.print("x: ");
        int x = input.nextInt();
        System.out.print("y: ");
        int y = input.nextInt();
        Point center = new Point(x, y);
        System.out.print("Радиус: ");
        double radius = input.nextDouble();
        Circle circle = new Circle(center, radius);
        System.out.println(circle);
        for(Point point: points){
            if(circle.includes(point)) {
                System.out.println(point);
            }
        }
    }
    public static Point[] setPoints(Scanner input) {
        Point[] points = new Point[10];
        for (int i = 0; i < 10; i++) {
            System.out.println("Input point: " + (i + 1));
            System.out.print("x: ");
            int x = input.nextInt();
            System.out.print("y: ");
            int y = input.nextInt();
            points[i] = new Point(x, y);
        }
        return points;
    }
}
